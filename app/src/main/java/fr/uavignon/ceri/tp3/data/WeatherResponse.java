package fr.uavignon.ceri.tp3.data;

import java.util.List;
import java.util.concurrent.locks.Condition;

public class WeatherResponse {
public final List<Weather> weather=null;
public final Long dt=null;
public final Clouds clouds=null;
public final Wind wind=null;
public final Main main=null;

    public static class Main  {
        public  Integer humidity;
        public  Float temp;


    }
 public static class Weather {
    public String description;
    public  String icon;


}
   public  static class Wind {
        public  Integer speed;


    }
 public    static class Clouds {
        public Integer All;

    }




}
